import socket
import struct
import os
import time
import sys
import logging

ROOT_DIR = os.path.abspath(os.path.join(__file__, os.pardir, os.pardir))
FILES_DIR = os.path.join(ROOT_DIR, 'messages')


logger = logging.getLogger('client')
hdlr = logging.FileHandler('client_{}_{}.log'.format(sys.argv[1], sys.argv[2]))
formatter = logging.Formatter('%(message)s')
hdlr.setFormatter(formatter)
logger.addHandler(hdlr)
logger.setLevel(logging.INFO)


class Client:
    def __init__(self, server_type, mechanism='ACK', ip='10.100.0.156', port=37137, p=0):
        self.server_type = server_type
        self.mechanism = mechanism
        self.ip = ip
        self.port = port
        self.p = p
        self.server_addr = (self.ip, self.port)
        if self.server_type == 'TCP':
            self.client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        elif self.server_type == 'UDP':
            self.client = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

    def run(self):
        start = time.time()
        messages_sent = 0

        if self.server_type == 'TCP':
            self.client.connect(self.server_addr)

        if self.mechanism == 'ACK':
            files = os.listdir(FILES_DIR)
            for f in files:
                content = open(os.path.join(FILES_DIR, f), 'rb').read()
                length_content = len(content)
                bytes_length = struct.pack('!H', length_content)
                message = bytes_length + content
                if self.server_type == 'TCP':
                    self.client.send(message)
                else:
                    self.client.sendto(content, self.server_addr)
                logger.info("Message sent: hash: {}, length: {}".format(f, length_content))
                logger.info("Waiting for ACK from server...")
                messages_sent += 1
                if self.server_type == 'TCP':
                    response = self.client.recv(3)
                else:
                    response, server = self.client.recvfrom(3)
                if response == b'ACK':
                    logger.info("====ACK====")
                    continue
        elif self.mechanism == 'STREAM':
            files = os.listdir(FILES_DIR)
            for f in files:
                content = open(os.path.join(FILES_DIR, f), 'rb').read()
                length_content = len(content)
                bytes_length = struct.pack('!H', length_content)
                if self.p == 1:
                    message = bytes_length + content
                else:
                    message = content
                if self.server_type == 'TCP':
                    self.client.send(message)
                else:
                    self.client.sendto(content, self.server_addr)
                logger.info("Message sent: hash: {}, length: {}".format(f, length_content))
                messages_sent += 1
        end = time.time()
        logger.info("Protocol used: {}, {}".format(self.server_type, self.mechanism))
        logger.info("Number of messages sent: {}".format(messages_sent))
        logger.info("Transmission time: {}".format(end-start))
        self.client.close()


client = Client(server_type=sys.argv[1], mechanism=sys.argv[2])
client.run()







