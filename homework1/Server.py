import socket
import threading
import struct
import hashlib
import time
import sys
import logging

logger = logging.getLogger('server')
hdlr = logging.FileHandler('server_{}_{}.log'.format(sys.argv[1], sys.argv[2]))
formatter = logging.Formatter('%(message)s')
hdlr.setFormatter(formatter)
logger.addHandler(hdlr)
logger.setLevel(logging.INFO)


class Server:
    def __init__(self, server_type, mechanism='ACK', ip='127.0.0.1', port=9999, chunk=1024, p=0):
        self.server_type = server_type
        self.mechanism = mechanism
        self.ip = ip
        self.port = port
        self.chunk = chunk
        self.p = p
        if self.server_type == 'TCP':
            self.server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        elif self.server_type == 'UDP':
            self.server = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

    def handle_client_connection_tcp(self, client_socket):
        number_messages_received = 0
        if self.mechanism == 'ACK':
            while True:
                msg = client_socket.recv(2)
                buffer_message = b''
                if msg != b'':
                    length_message = struct.unpack('!H', msg)[0]
                    logger.info("=" * 20)
                    logger.info("Bytes to read: {}".format(length_message))
                    while length_message >= self.chunk:
                        message = client_socket.recv(self.chunk)
                        buffer_message += message
                        length_message -= self.chunk
                    message = client_socket.recv(length_message)
                    buffer_message += message
                    number_messages_received += 1
                    logger.info("Bytes read: {}, MD5 checksum: {}".format(len(buffer_message),
                                                                          hashlib.md5(buffer_message).hexdigest()))
                    client_socket.send(b'ACK')
                else:
                    break
        elif self.mechanism == 'STREAM':
            while True:
                msg = client_socket.recv(2)
                buffer_message = b''
                if msg != b'':
                    if self.p == 1:
                        length_message = struct.unpack('!H', msg)[0]
                        logger.info("=" * 20)
                        logger.info("Bytes to read: {}".format(length_message))
                        while length_message >= self.chunk:
                            message = client_socket.recv(self.chunk)
                            buffer_message += message
                            length_message -= self.chunk
                        message = client_socket.recv(length_message)
                        buffer_message += message
                        number_messages_received += 1
                        logger.info("Bytes read: {}, MD5 checksum: {}".format(len(buffer_message),
                                                                              hashlib.md5(buffer_message).hexdigest()))
                    else:
                        buffer_message = client_socket.recv(65535)
                        number_messages_received += 1
                        logger.info("Bytes read: {}, MD5 checksum: {}".format(len(buffer_message),
                                                                       hashlib.md5(buffer_message).hexdigest()))

                else:
                    break
        logger.info("Protocol used: {}, {}".format(self.server_type, self.mechanism))
        logger.info("Number of messages received: {}".format(number_messages_received))
        client_socket.close()

    def handle_client_connection_udp(self):
        number_messages_received = 0
        #self.server.settimeout(10)
        if self.mechanism == 'ACK':
            while True:
                msg, address = self.server.recvfrom(65535)
                if msg != b'':
                    number_messages_received += 1
                    logger.info("Msg Nr: {}, Bytes read: {}, MD5 checksum: {}".format(number_messages_received, len(msg),
                                                                          hashlib.md5(msg).hexdigest()))
                    self.server.sendto(b'ACK', address)
                print(len(msg))
        if self.mechanism == 'STREAM':
            while True:
                msg, address = self.server.recvfrom(65535)
                if msg != b'':
                    number_messages_received += 1
                    logger.info("Msg Nr: {}, Bytes read: {}, MD5 checksum: {}".format(number_messages_received,
                                                                             len(msg), hashlib.md5(msg).hexdigest()))
                print(len(msg))
        print("Protocol used: {}, {}".format(self.server_type, self.mechanism))
        print("Number of messages received: {}".format(number_messages_received))

    def run(self):
        self.server.bind((self.ip, self.port))
        if self.server_type == 'TCP':
            self.server.listen(5)
        while True:
            if self.server_type == 'TCP':
                client_sock, address = self.server.accept()
                print('Accepted connection from {}:{}'.format(address[0], address[1]))
                client_handler = threading.Thread(target=self.handle_client_connection_tcp, args=(client_sock,))
                client_handler.start()
            elif self.server_type == 'UDP':
                self.handle_client_connection_udp()


server = Server(server_type=sys.argv[1], mechanism=sys.argv[2])
server.run()



