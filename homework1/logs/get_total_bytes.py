﻿import sys
import re


content = open(sys.argv[1], 'r').readlines()
total = 0
for line in content:
	match = re.findall(r'Bytes read: (\d+)', line)
	#match = re.findall(r'length: (\d+)', line)
	if len(match):
		total += int(match[0])
print(total)